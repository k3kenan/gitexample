# functions

# f(x)=x*2+2
# f(2)=6

def f(x):
    print(x*2+2)

f(2)
f(24)
f(90)
f(1000)

def square_area_calculator(edge):
    # please enter edge in cm
    # print(r*r)
    print(edge**2,"cm^2")

square_area_calculator(10)
